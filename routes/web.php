<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('blade', 'Controller@blade');

Route::group(['middleware' => 'auth'], function() {

    Route::get('/shoppingLists/generate', 'ShoppingListsController@generate')->name('shoppingLists.generate');

    Route::resource('/shoppingLists', 'ShoppingListsController');
    Route::resource('/shoppingLists.products', 'ShoppingLists\ProductsController');

    Route::resource('/schedules', 'schedulesController');
});