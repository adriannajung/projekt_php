<header id="header">
    <div class="containerMenu">

        <div id="logo" class="pull-left">
            <h1><a href="/" class="scrollto">ShopUp!</a></h1>
            <!-- Uncomment below if you prefer to use an image logo -->
            <!-- <a href="#intro"><img src="img/logo.png" alt="" title=""></a> -->
        </div>

        <nav id="nav-menu-container">

            <ul class="nav-menu">
                <li><a href="{{ route('shoppingLists.generate') }}">Generate New List</a></li>
                <li><a href="{{ route('schedules.create') }}">Plan Buying Schedule</a></li>
                <li><a href="{{ route('shoppingLists.index') }}">Shopping History</a></li>
                <li><a></a></li>
                @guest
                    <li><a href="{{ route('login') }}">Login</a></li>
                    <li><a href="{{ route('register') }}">Register</a></li>
                    @else
                        <li class="dropdown" style="height:2.4vh">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                        @endguest
            </ul>
        </nav><!-- #nav-menu-container -->
    </div>

</header><!-- #header -->