@extends('layouts.master')

@section('centered')
<div class="containerWhite">
    <div style="float:left;margin-left:4vh;margin-top:17vh;font-family: 'Montserrat', sans-serif;color:white;">
    <h2>New schedule:</h2>

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form method="post" action="{{ route('schedules.store') }}">
        {{ csrf_field() }}
        Product name: <input type="text" name="product_name" value="{{ old("product_name") }}">
        <br>
        <br>
        Buying interval in days: <input type="text" name="buying_interval" value="{{ old("buying_interval") }}">
        <br>
        <br>
        <input type="submit" value="Create">
    </form>
    </div>
    <div style="margin-left:78vh;margin-top:3vh;width:30%;font-family: 'Montserrat', sans-serif;color:#fff ">
    <h3>Current schedules:</h3>
    </div>
        <div style="float:left;height:65vh;background:white; margin-left: 26.3vh;width:24%;font-family: 'Montserrat', sans-serif;color:slategrey;-webkit-border-radius:2vh;overflow-y:auto">
    <ul>
        @foreach($user->schedules as $schedule)
            <li>
                <strong>{{ $schedule->product_name }} buy every: {{ $schedule->buying_interval / (60*60*24)}} day(s)</strong>

                {{ Form::open(['method' => 'DELETE', 'route' => ['schedules.destroy', $schedule->id]]) }}
                {{ Form::submit('Delete', ['class' => 'btn btn-danger']) }}
                {{ Form::close() }}
            </li>
        @endforeach
    </ul>
    </ul>
    </div>
</div>

</div>
@endsection