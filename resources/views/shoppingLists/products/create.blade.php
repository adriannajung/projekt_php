@extends('layouts.master')

@section('centered')
<div class="containerWhite">
    <div style="float:left;margin-left:4vh;margin-top:17vh;font-family: 'Montserrat', sans-serif;color:white;">
    <h2>New shopping list:</h2>

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


    <form method="post" action="{{ route('shoppingLists.products.store', $shoppingList) }}">
        {{ csrf_field() }}
        Name: <input type="text" name="name" value="{{ old("name") }}">
        <br>
        <br/>
        <input type="submit" value="Add Item">
    </form>
        <br>
        <a href="{{ route('shoppingLists.show', $shoppingList) }}" style="color:blue">Store shopping list</a>
    </div>
<div style="margin-left:78vh;margin-top:3vh;width:30%;font-family: 'Montserrat', sans-serif;color:#fff ">
    <h3>Current list:</h3>
</div>
<div style="float:left;height:65vh;background:white; margin-left: 42vh;width:24%;font-family: 'Montserrat', sans-serif;color:slategrey;-webkit-border-radius:2vh;overflow-y:auto">
    <ul>
        @foreach($shoppingList->products as $product)
            <li>
                <strong>{{ $product->name }}</strong>

                {{ Form::open(['method' => 'DELETE', 'route' => ['shoppingLists.products.destroy', $product->id, $shoppingList]]) }}
                    {{ Form::submit('Delete', ['class' => 'btn btn-danger']) }}
                {{ Form::close() }}
            </li>
        @endforeach
    </ul>
</div>
</div>
@endsection