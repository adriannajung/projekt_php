@extends('layouts.master')

@section('centered')
<div class="containerWhite">
    <div style="margin-left:78vh;margin-top:3vh;width:30%;font-family: 'Montserrat', sans-serif;color:#fff ">
    <h3>Shopping History:</h3>
    </div>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div style="float:left;margin-left:17vh;margin-top:17vh;font-family: 'Montserrat', sans-serif;color:white;">
    <a class="button" href="{{ route('shoppingLists.create') }}">Add last shopping list</a>
    </div>

    <div style="float:left;height:65vh;background:white; margin-left: 39.7vh;width:24%;font-family: 'Montserrat', sans-serif;color:slategrey;-webkit-border-radius:2vh;overflow-y:auto">
    @foreach($user->shoppingLists->reverse() as $shoppingList)
        <ul>
            @foreach($shoppingList->products as $product)
                <li>
                    <strong>{{ $product->name }}</strong>
                </li>
            @endforeach
            <br>
        </ul>
    @endforeach
    </div>
</div>
@endsection
