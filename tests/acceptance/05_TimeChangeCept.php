<?php
$I = new AcceptanceTester($scenario);
$I->wantTo('time variations');

$id=1;
$I->dontSeeInDatabase("schedules", ["user_id"=> $id]);

$myName = 'example_name';
$myEmail = 'example@email.com';
$myPassword = 'example_password';

$time=time();


$I->haveInDatabase('users', [
    'email' => $myEmail,
    'name' => $myName,
    'password' => password_hash($myPassword, PASSWORD_DEFAULT),
    'id'=>$id
]);

$I->amOnPage('/');

$I->click("Login");
$I->seeCurrentUrlEquals("/login");

$I->fillField('email', $myEmail);
$I->fillField('password', $myPassword);

$I->click('button[type=submit]');

$buyingInterval=100;
$name="example product";

$I->haveInDatabase('schedules', [
    'buying_interval' => $buyingInterval,
    'user_id'=>$id,
    'product_name'=>$name
]);

$I->haveInDatabase('shopping_lists', [
    'user_id'=>$id,
]);

$idList = $I->grabFromDatabase('shopping_lists', 'id', [
    'user_id' => $id,
]);


$I->haveInDatabase("products", [
    "user_id"=>$id,
    "shopping_list_id"=> (int) $idList,
    "name"=> $name,
    'time'=>$time
]);


$I->haveInDatabase('shopping_lists', [
    'user_id'=>$id,
]);


$idList = $I->grabFromDatabase('shopping_lists', 'id', [
    'user_id' => $id,
    'id' => (int) $idList+1
]);


$I->haveInDatabase("products", [
    "user_id"=>$id,
    "shopping_list_id"=> (int) $idList,
    "name"=> $name,
    'time'=> $time
]);


$I->click('Generate New List');

$I->see('Current list:', 'h3');

$I->dontSee($name, 'strong');