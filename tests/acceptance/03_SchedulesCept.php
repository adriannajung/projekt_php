<?php
$I = new AcceptanceTester($scenario);
$I->wantTo('add schedule, check if exist in database');

$id=1;
$I->dontSeeInDatabase("schedules", ["user_id"=> $id]);

$myName = 'example_name';
$myEmail = 'example@email.com';
$myPassword = 'example_password';


$I->haveInDatabase('users', [
    'email' => $myEmail,
    'name' => $myName,
    'password' => password_hash($myPassword, PASSWORD_DEFAULT),
    'id'=>$id
]);

$I->amOnPage('/');

$I->click("Login");
$I->seeCurrentUrlEquals("/login");

$I->fillField('email', $myEmail);
$I->fillField('password', $myPassword);

$I->click('button[type=submit]');

$buyingInterval=0;
$name="example product";
$myId=$I->grabFromDatabase('users', 'id', [
    'id' => $id
]);

$I->haveInDatabase('schedules', [
    'buying_interval' => $buyingInterval,
    'user_id'=>$myId,
    'product_name'=>$name
]);


$I->haveInDatabase('shopping_lists', [
    'user_id'=>$myId,
]);


$idList = $I->grabFromDatabase('shopping_lists', 'id', [
    'user_id' => $id,
]);

$name="example product";
$time=(time()-10);


$I->haveInDatabase("products", [
    "user_id"=>$myId,
    "shopping_list_id"=> (int) $idList,
    "name"=> $name,
    'time'=>$time
]);


$I->haveInDatabase('shopping_lists', [
    'user_id'=>$myId,
]);


$idList = $I->grabFromDatabase('shopping_lists', 'id', [
    'user_id' => $id,
    'id' => (int) $idList+1
]);

$time2=time();

$I->haveInDatabase("products", [
    "user_id"=>$myId,
    "shopping_list_id"=> (int) $idList,
    "name"=> $name,
    'time'=> $time2
]);


$I->click('Generate New List');

$I->see('Current list:', 'h3');

$I->see($name, 'strong');