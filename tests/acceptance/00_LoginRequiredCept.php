<?php 
$I = new AcceptanceTester($scenario);
$I->wantTo('see standard homepage');

$I->amOnPage('/');

$I->seeInTitle('ShopUp!');

$I->wantTo('dont see hidden page when logout');

$I->click("Shopping History");
$I->seeCurrentUrlEquals("/login");

$I->click("Shopping History");
$I->seeCurrentUrlEquals("/login");

$I->click("Generate New List");
$I->seeCurrentUrlEquals("/login");

$I->click("Plan Buying Schedule");
$I->seeCurrentUrlEquals("/login");

$I->click("Login");
$I->seeCurrentUrlEquals("/login");

$I->click("Register");
$I->seeCurrentUrlEquals("/register");
