<?php
$I = new AcceptanceTester($scenario);
$I->wantTo('add new lists, check if older exist');

$id=1;
$I->dontSeeInDatabase("shopping_lists", ["user_id"=> $id]);

$myName = 'example_name';
$myEmail = 'example@email.com';
$myPassword = 'example_password';


$I->haveInDatabase('users', [
    'email' => $myEmail,
    'name' => $myName,
    'password' => password_hash($myPassword, PASSWORD_DEFAULT),
    'id'=>$id
]);

$I->amOnPage('/');

$I->click("Generate New List");
$I->seeCurrentUrlEquals("/login");

$I->fillField('email', $myEmail);
$I->fillField('password', $myPassword);

$I->click('button[type=submit]');

$idList = $I->grabFromDatabase('shopping_lists', 'id', [
    'user_id' => $id
]);

$I->seeCurrentUrlEquals('/shoppingLists/'. $idList .'/products/create');

$I->see('New shopping list:', 'h2');
$I->see('Current List', 'h3');

$I->seeInDatabase("shopping_lists", ["user_id"=> $id]);

$name = "example product";

$I->fillField('name', $name);

$I->click('Add');

$I->see('example product', 'strong');

$I->seeInDatabase("products", [
    "user_id"=>$id,
    "shopping_list_id"=>(int) $idList,
    "name"=>$name
]);

$I->click('Store shopping list');

$I->seeInCurrentUrl("/shoppingLists");

$I->see('Shopping History:', 'h3');

$I->see($name, 'strong');

$I->click("Generate New List");

$I->see('New shopping list:', 'h2');
$I->see('Current List', 'h3');

$I->seeInDatabase("shopping_lists", ["user_id"=> $id, 'id' => (int) $idList + 1]);

$name2 = 'example product 2';

$I->fillField('name', $name2);

$I->click('Add');

$I->see($name2, 'strong');

$idList = $I->grabFromDatabase('shopping_lists', 'id', [
    'user_id' => $id,
    'id' => (int) $idList + 1
]);

$I->seeInDatabase("products", [
    "user_id"=>$id,
    "shopping_list_id"=> (int) $idList,
    "name"=> $name2
]);

$I->click('Store shopping list');

$I->seeInCurrentUrl("/shoppingLists");

$I->see('Shopping History:', 'h3');

$I->see($name, 'strong');

$I->see($name2, 'strong');

$I->click('Shopping History');

$I->seeInCurrentUrl("/shoppingLists");

$I->see('Shopping History:', 'h3');

$I->see($name, 'strong');

$I->see($name2, 'strong');