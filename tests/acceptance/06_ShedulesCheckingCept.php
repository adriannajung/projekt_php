<?php 
$I = new AcceptanceTester($scenario);
$I->wantTo('check my shopping list');

$id=1;
$I->dontSeeInDatabase("schedules", ["user_id"=> $id]);

$myName = 'example_name';
$myEmail = 'example@email.com';
$myPassword = 'example_password';

$name='example product';
$time=1;

$I->haveInDatabase('users', [
    'email' => $myEmail,
    'name' => $myName,
    'password' => password_hash($myPassword, PASSWORD_DEFAULT),
    'id'=>$id
]);

$I->amOnPage('/');

$I->click('Plan Buying Schedule');
$I->seeCurrentUrlEquals("/login");

$I->fillField('email', $myEmail);
$I->fillField('password', $myPassword);

$I->click('button[type=submit]');



$idList = $I->grabFromDatabase('schedules', 'id', [
    'user_id' => $id
    ]);

$I->haveInDatabase("schedules", [
    "user_id"=> $id,
    'id'=>$idList,
    "product_name"=>$name,
    "buying_interval"=>$time
    ]);

$idList = $I->grabFromDatabase('schedules', 'id', [
    'user_id' => $id,
    'id' => (int) $idList + 1
]);

$I->haveInDatabase("schedules", [
    "user_id"=> $id,
    'id'=>$idList + 1,
    "product_name"=>$name,
    "buying_interval"=>$time
]);

$I->click('Plan Buying Schedule');

//$I->see('New schedule:', 'h2');

$I->see('Current schedules:', 'h3');

$I->see( $name . ' buy every: ' . ($time/(60*60*24)) . ' day(s)', 'strong');

$I->see( $name . ' buy every: ' . ($time/(60*60*24)) . ' day(s)', 'strong');