<?php

namespace App\Http\Controllers\ShoppingLists;


use App\Product;
use App\ShoppingList;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ShoppingList $shoppingList)
    {
        return view('shoppingLists.products.create')->withShoppingList($shoppingList);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(ShoppingList $shoppingList)
    {
        return view('shoppingLists.products.create')->withShoppingList($shoppingList);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, ShoppingList $shoppingList)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        $product = new Product();
        $product->name = strtolower($request->name);
        $product->user_id = $shoppingList->user_id;
        $product->time = time();

        $shoppingList->products()->save($product);

        return redirect()->route('shoppingLists.products.create', $shoppingList);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(ShoppingList $shoppingList)
    {
        return view('shoppingLists.products.create')->withShoppingList($shoppingList);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, $shoppingList)
    {
        Product::destroy($id);

        return redirect()->route('shoppingLists.products.create', $shoppingList);
    }
}
