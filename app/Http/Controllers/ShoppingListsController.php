<?php

namespace App\Http\Controllers;

use App\schedule;
use App\ShoppingList;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class ShoppingListsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();

        foreach ($user->shoppingLists->reverse() as $shoppingList) {
            if (!$shoppingList->products()->count()) {
                $shoppingList->delete();
            }
        }

        return view('shoppingLists.index')->withUser($user);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        $shoppingList = new ShoppingList();
        $shoppingList->user_id = $user->id;
        $user->shoppingLists()->save($shoppingList);
        return redirect()->route('shoppingLists.products.create', $shoppingList);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
       //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(ShoppingList $shoppingList)
    {
        if (!$shoppingList->products()->count()) {
            $shoppingList->delete();
        }

        return redirect()->route('shoppingLists.index');
    }

    public function findUniqueProducts($user){
        $res = Product::where('user_id', $user->id)->get();
        $products = [];

        foreach ($res as $pro){
            if (array_key_exists($pro->name, $products)){
                array_push($products[$pro->name], $pro);
            } else {
                $products[$pro->name] = [$pro];
            }
        }

        return $products;
    }

    public function getNeededProducts($products){
        $res_pro = [];
        $now = time();
        $user = Auth::user();

        foreach ($products as $pro){
            $len = count($pro);
            $schedule = schedule::where('product_name', $pro[0]->name)->where('user_id', $user->id)->first();

            if ($schedule){
                if ($now - $pro[$len - 1]->time >= $schedule->buying_interval){
                    array_push($res_pro, $pro[0]);
                }
            } else {
                if ($len > 1) {
                    if ($pro[$len - 1]->time - $pro[$len - 2]->time <= $now - $pro[$len - 1]->time) {
                        array_push($res_pro, $pro[0]);
                    }
                }
            }
        }

        return $res_pro;
    }

    public function generate()
    {
        $user = Auth::user();
        $shoppingList = new ShoppingList();
        $shoppingList->user_id = $user->id;
        $user->shoppingLists()->save($shoppingList);

        $products = $this->findUniqueProducts($user);
        $products = $this->getNeededProducts($products);

        foreach ($products as $pro) {
            $res_pro = new Product();
            $res_pro->name = $pro->name;
            $res_pro->user_id = $pro->user_id;
            $res_pro->time = time();
            $shoppingList->products()->save($res_pro);
        }

        return redirect()->route('shoppingLists.products.create', $shoppingList);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
