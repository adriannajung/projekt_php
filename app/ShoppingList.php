<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShoppingList extends Model
{
    function products()
    {
        return $this->hasMany(Product::class);
    }
}
